﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficSimulation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MonoGamePhase2 
{
     public class TotalSprite : DrawableGameComponent
     {
        private SpriteBatch spriteBatch;
        private Texture2D rect;
        private Simulation game;
        private TrafficControl tc;
        private SpriteFont font;
        private Microsoft.Xna.Framework.Vector2 fontPosition;
        
        public TotalSprite(Simulation game,TrafficControl tc) : base(game)
        {
            this.game = game;
            this.tc = tc;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // Specify font you want to use
            font = game.Content.Load<SpriteFont>("File");
            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            // What you want to display
            string output = "Total Number of Vehicles: " + tc.Total.TotalVehicle + "\n" +
                            "Total Passengers: " + tc.Total.Passengers +"\n"+
                            "Total Emissions: " + tc.Total.Emissions;

            //spriteBatch.Draw(rect,new Rectangle(0,tc.Grid.Size-1,tc.Grid.Size-1,56),Color.White);
            spriteBatch.DrawString(font, output, new Microsoft.Xna.Framework.Vector2(tc.Grid.Size * 30, 20), Color.Green);
       

            spriteBatch.End();

            base.Draw(gameTime);
        }

    }//end class
}//end namespace
