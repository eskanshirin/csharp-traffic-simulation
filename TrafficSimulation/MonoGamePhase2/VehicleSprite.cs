﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficSimulation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MonoGamePhase2 
{
     public class VehicleSprite : DrawableGameComponent
    {
        private Simulation game;
        private Texture2D carDown, carLeft, carRight, carUp, motorcycleDown, motorcycleLeft, motorcycleRight, motorcycleUp;
        private SpriteBatch spriteBatch;
        private TrafficControl tc;

        public VehicleSprite(Simulation game, TrafficControl tc) : base(game)
        {
            this.tc = tc;
            this.game = game;
        }
        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load pictures
            carDown = game.Content.Load<Texture2D>("carDown");
            carLeft = game.Content.Load<Texture2D>("carLeft");
            carRight = game.Content.Load<Texture2D>("carRight");
            carUp = game.Content.Load<Texture2D>("carUp");
            motorcycleDown = game.Content.Load<Texture2D>("motorcycleDown");
            motorcycleUp = game.Content.Load<Texture2D>("motorcycleUp");
            motorcycleRight = game.Content.Load<Texture2D>("motorcycleRight");
            motorcycleLeft = game.Content.Load<Texture2D>("motorcycleLeft");

            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            tc.Update();

            
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            foreach (var vehicle in tc.Intersection)
            {
                if (vehicle.Passengers == 3)
                {
                    if (vehicle.Direction == Direction.Down)
                    {
                        spriteBatch.Draw(carDown, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Up)
                    {
                        spriteBatch.Draw(carUp, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Right)
                    {
                        spriteBatch.Draw(carRight, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Left)
                    {
                        spriteBatch.Draw(carLeft, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                }
                else
                {
                    if (vehicle.Direction == Direction.Down)
                    {
                        spriteBatch.Draw(motorcycleDown, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Up)
                    {
                        spriteBatch.Draw(motorcycleUp, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Right)
                    {
                        spriteBatch.Draw(motorcycleRight, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                    else if (vehicle.Direction == Direction.Left)
                    {
                        spriteBatch.Draw(motorcycleLeft, new Rectangle(vehicle.Y * 30, vehicle.X * 30, 30, 30), Color.White);
                    }
                }
                
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
