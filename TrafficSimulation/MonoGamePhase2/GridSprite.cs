﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrafficSimulation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MonoGamePhase2
{
    public class GridSprite : DrawableGameComponent
    {
        private TrafficControl tc; 
                               
        private SpriteBatch spriteBatch;
        private Texture2D grass,roadup,roaddown,roadleft,
                           roadright,intersection,red,yellow,green;
        private Simulation game;

        public GridSprite(Simulation game, TrafficControl tc) : base(game)
        {
            this.game = game;
            this.tc = tc;
        }
        public override void Initialize()
        {
            //ask!
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            grass = game.Content.Load<Texture2D>("grass");

            roadup = game.Content.Load<Texture2D>("roadup");
            roaddown = game.Content.Load<Texture2D>("roaddown");
            roadleft = game.Content.Load<Texture2D>("roadleft");
            roadright = game.Content.Load<Texture2D>("roadright");

            intersection = game.Content.Load<Texture2D>("intersection");

            red = game.Content.Load<Texture2D>("red");
            yellow = game.Content.Load<Texture2D>("yellow");
            green = game.Content.Load<Texture2D>("green");
            base.LoadContent();
         


        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();

            for(int i = 0; i < tc.Grid.Size; i++)
            {
              for (int j = 0; j < tc.Grid.Size; j++)
                {
                    if(tc.Grid[i, j] is Grass)
                    {
                        spriteBatch.Draw(grass, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                    }
                    if(tc.Grid[i, j] is IntersectionTile)
                    {
                        spriteBatch.Draw(intersection, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                    }
                    if(tc.Grid[i, j] is Road)
                    {
                        if(tc.Grid[i, j].Direction== Direction.Down)
                        {
                            spriteBatch.Draw(roaddown, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                        }
                        if (tc.Grid[i, j].Direction == Direction.Up)
                        {
                            spriteBatch.Draw(roadup, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                        }
                        if (tc.Grid[i, j].Direction == Direction.Left)
                        {
                            spriteBatch.Draw(roadleft, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                        }
                        if (tc.Grid[i, j].Direction == Direction.Right)
                        {
                            spriteBatch.Draw(roadright, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                        }
                    }//if is road

                        if(tc.Grid[i, j] is Light)
                        {
                            if ( ( ((Light)tc.Grid[i, j]).Colour == Colour.Red ) )
                            {
                                spriteBatch.Draw(red, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                            }
                            else if ( ( ((Light)tc.Grid[i, j]).Colour == Colour.Green) )
                            {
                                spriteBatch.Draw(green, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                            }
                            else if ( ( ((Light)tc.Grid[i, j]).Colour == Colour.Amber) )
                            {
                                spriteBatch.Draw(yellow, new Rectangle(j * 30, i * 30, 30, 30), Color.White);

                            }
                        }
                } 
            }

            spriteBatch.End();  
        }// end draw
    }// end class
    
}// end namespace
