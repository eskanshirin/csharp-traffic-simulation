﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;
using Microsoft.Xna.Framework;

namespace TrafficControlTesting
{
    [TestClass]
    public class TrafficControlTesting
    {
        [TestMethod]
        public void TestParse()
        {
            TrafficControl tc = new TrafficControl();

            string[] lines = {"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G G D U G G G G",
                            "G G D U G G G G",
                            "G 1 D U 2 G G G",
                            "L L I I L L L L",
                            "R R I I R R R R",
                            "G 3 D U 4 G G G",
                            "G G D U G G G G",
                            "G G D U G G G G",
                            };
            String fileContent = "10" + "\n" +
                           "2" + "\n" +
                           "50" + "\n" +
                           "20" + "\n" +
                           "1 1 1 1" + "\n" +
                           "G G D U G G G G" + "\n" +
                           "G G D U G G G G" + "\n" +
                           "G 1 D U 2 G G G" + "\n" +
                           "L L I I L L L L" + "\n" +
                           "R R I I R R R R" + "\n" +
                           "G 3 D U 4 G G G" + "\n" +
                           "G G D U G G G G" + "\n" +
                           "G G D U G G G G";

            tc.Parse(fileContent);

            //testing the getters
            Assert.AreEqual(tc.numVehicles, 10);//like in my file
            Assert.AreEqual(tc.percentCars, 50);//like in my file
            Assert.AreEqual(tc.percentElectric, 20);//like in my file
            Assert.AreEqual(tc.delay, 2);
            //testing the CreateTotal()
            Assert.AreEqual(tc.Total.TotalVehicle, 10);//like in my file           
        }
        [TestMethod]
        public void TestCreateIntersection()
        {
            TrafficControl tc = new TrafficControl();
            string[] linesParsed = {"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G G D U G G G G",
                            "G G D U G G G G",
                            "G 1 D U 2 G G G",
                            "L L I I L L L L",
                            "R R I I R R R R",
                            "G 3 D U 4 G G G",
                            "G G D U G G G G",
                            "G G D U G G G G",
                            };
            tc.CreateGrid(linesParsed);
            tc.CreateIntersection(linesParsed);

            //test if the created points are done well
            Assert.AreEqual(tc[0], new Vector2(0, 2));
            Assert.AreEqual(tc[1], new Vector2(3, 7));
            Assert.AreEqual(tc[2], new Vector2(4, 0));
            Assert.AreEqual(tc[3], new Vector2(7, 3));

        }

        [TestMethod]
        public void TestCreateSignal()
        {
            TrafficControl tc = new TrafficControl();

            string[] linesParsed ={"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G G D U G G",
                            "G G D U G G",
                            "G 1 D U 2 G",
                            "L L I I L L",
                            "R R I I R R",
                            "G 3 D U 4 G",
                            "G G D U G G",
                            "G G D U G G",
                            };
            tc.CreateSignal(linesParsed);

            Assert.AreEqual(tc.signal.TimingToString(), "1 1 1 1");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCreateGrid()
        {
            TrafficControl tc = new TrafficControl();
            TrafficControl tcMini = new TrafficControl();

            string[] linesPass = {"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G L D U I R 1"
                            };
            tcMini.CreateGrid(linesPass);

            //checking the length 
            Assert.AreEqual(tc.Grid.Size, 8);
            Assert.AreEqual(tc.Grid.Size, 6);
            Assert.IsInstanceOfType(tcMini.Grid[0, 0], typeof(Grass));
            Assert.IsInstanceOfType(tcMini.Grid[0, 1], typeof(Road));
            Assert.IsInstanceOfType(tcMini.Grid[0, 2], typeof(Road));
            Assert.IsInstanceOfType(tcMini.Grid[0, 3], typeof(Road));
            Assert.IsInstanceOfType(tcMini.Grid[0, 4], typeof(Intersection));
            Assert.IsInstanceOfType(tcMini.Grid[0, 5], typeof(Road));
            Assert.IsInstanceOfType(tcMini.Grid[0, 6], typeof(Light));

        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestUnexistantLightNumberFail()
        {
            //should throw an exception
            TrafficControl tcMini = new TrafficControl();
            string[] linesLightFail = {"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G L D U I R 1034"
                            };

            tcMini.CreateGrid(linesLightFail);

            Assert.IsInstanceOfType(tcMini.Grid[0, 6], typeof(Light));

        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestUnexistantDirectionFail()
        {
            TrafficControl tcMini = new TrafficControl();
            string[] linesDirectionFail = {"10",
                            "2",
                            "50",
                            "20",
                            "1 1 1 1",
                            "G L D U I P 2"
                            };
            //should throw an exception
            tcMini.CreateGrid(linesDirectionFail);
            Assert.IsInstanceOfType(tcMini.Grid[0, 6], typeof(Road));
        }
    }
}