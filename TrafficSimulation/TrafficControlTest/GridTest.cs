﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;
using Microsoft.Xna.Framework;

namespace TrafficControlTest
{
    [TestClass]
    public class GridTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();

            // Act
            Grid grid = new Grid(tiles);
            // Assert
           Assert.IsInstanceOfType(grid[0, 0], typeof(Grass));
           Assert.IsInstanceOfType(grid[2, 2], typeof(IntersectionTile));

        }// End Test Constructor method

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorException()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();
            Tile[,] smallGrid = new Tile[3, 3];

            for (int i = 0; i < smallGrid.GetLength(0); i++)
            {
                for (int j = 0; j < smallGrid.GetLength(1); j++)
                {
                    smallGrid[i, j] = tiles[i, j];
                }
            }

            // Act // Assert
            Grid grid = new Grid(smallGrid);
        }

        [TestMethod]
        public void TestIndexer()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();
            Grid grid = new Grid(tiles);

            // Act // Assert
            Assert.IsInstanceOfType(grid[1, 1], typeof(Road));
        }

        [TestMethod]
        public void TestSize()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();
            Grid grid = new Grid(tiles);

            // Act  // Assert
            Assert.AreEqual(grid.Size, 20);

        }// End TestSize method

        [TestMethod]
        public void TestIsOccupied()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();
            Grid grid = new Grid(tiles);

            // Act
            grid[1, 3].Occupied = true;

            // Assert
            Assert.IsTrue(grid.IsOccupied(1, 3));
            Assert.IsFalse(grid.IsOccupied(0, 4));
            Assert.IsFalse(grid.IsOccupied(0, 0));
        }

        [TestMethod]
        public void TestInBounds()
        {
            // Arrange
            Tile[,] tiles = this.CreateArrayTiles();
            Grid grid = new Grid(tiles);

            // Act // Assert
            Assert.IsTrue(grid.InBounds(0, 0));
            Assert.IsTrue(grid.InBounds(5, 5));
            Assert.IsTrue(grid.InBounds(2, 5));
            Assert.IsTrue(grid.InBounds(5, 2));

            Assert.IsFalse(grid.InBounds(3, 6));
            Assert.IsFalse(grid.InBounds(7, 3));
            Assert.IsFalse(grid.InBounds(-1, 2));
        }

        private Tile[,] CreateArrayTiles()
        {
            // Create FixedSignal
            int[] timing = new int[] { 20, 5, 15, 5 };
            FixedSignal signal = new FixedSignal(timing);

            Tile[,] tiles = new Tile[6, 6];

            tiles[0, 0] = new Grass();
            tiles[0, 1] = new Grass();
            tiles[0, 2] = new Road(Direction.Right);
            tiles[0, 3] = new Road(Direction.Left);
            tiles[0, 4] = new Grass();
            tiles[0, 5] = new Grass();

            tiles[1, 0] = new Grass();
            tiles[1, 1] = new Light(signal, Direction.Right);
            tiles[1, 2] = new Road(Direction.Right);
            tiles[1, 3] = new Road(Direction.Left);
            tiles[1, 4] = new Light(signal, Direction.Down);
            tiles[1, 5] = new Grass();

            tiles[2, 0] = new Road(Direction.Down);
            tiles[2, 1] = new Road(Direction.Down);
            tiles[2, 2] = new IntersectionTile();
            tiles[2, 3] = new IntersectionTile();
            tiles[2, 4] = new Road(Direction.Down);
            tiles[2, 5] = new Road(Direction.Down);

            tiles[3, 0] = new Road(Direction.Up);
            tiles[3, 1] = new Road(Direction.Up);
            tiles[3, 2] = new IntersectionTile();
            tiles[3, 3] = new IntersectionTile();
            tiles[3, 4] = new Road(Direction.Up);
            tiles[3, 5] = new Road(Direction.Up);

            tiles[4, 0] = new Grass();
            tiles[4, 1] = new Light(signal, Direction.Up);
            tiles[4, 2] = new Road(Direction.Right);
            tiles[4, 3] = new Road(Direction.Left);
            tiles[4, 4] = new Light(signal, Direction.Left);
            tiles[4, 5] = new Grass();

            tiles[5, 0] = new Grass();
            tiles[5, 1] = new Grass();
            tiles[5, 2] = new Road(Direction.Right);
            tiles[5, 3] = new Road(Direction.Left);
            tiles[5, 4] = new Grass();
            tiles[5, 5] = new Grass();

            return tiles;
        }// End CreateGrid method
    }// End GridTest class
}
