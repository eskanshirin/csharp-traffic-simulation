﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace TrafficControlTest
{
    [TestClass]
    public class IntersectionTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            // Arrange
            FixedSignal signal = this.CreateSignal();
            Grid grid = this.CreateGrid(signal);
            
            List<Vector2> startCoords = new List<Vector2>();
            startCoords.Add(new Vector2(0, 2));
            startCoords.Add(new Vector2(2, 5));
            startCoords.Add(new Vector2(3, 0));
            startCoords.Add(new Vector2(5, 3));

            // Act
            Intersection intersection = new Intersection(signal, startCoords, grid);

            // Assert
            Assert.IsInstanceOfType(intersection, typeof(Intersection));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestConstructorException()
        {
            // Arrange
            FixedSignal signal = this.CreateSignal();
            Grid grid = this.CreateGrid(signal);
            
            List<Vector2> startCoords = new List<Vector2>();
            startCoords.Add(new Vector2(0, 2));
            startCoords.Add(new Vector2(2, 5));
            startCoords.Add(new Vector2(3, 0));
            startCoords.Add(new Vector2(5, 3));

            grid[0, 2].Occupied = true;

            // Arrange // Act
            Intersection intersection = new Intersection(signal, startCoords, grid);
        }// End 

        [TestMethod]
        public void TestAdd()
        {
            // Arrange 
            FixedSignal signal = this.CreateSignal();
            Grid grid = this.CreateGrid(signal);
            
            List<Vector2> startCoords = new List<Vector2>();
            startCoords.Add(new Vector2(0, 2));
            startCoords.Add(new Vector2(2, 5));
            startCoords.Add(new Vector2(3, 0));
            startCoords.Add(new Vector2(5, 3));
            Intersection intersection = new Intersection(signal, startCoords, grid);

            // Act // 
            Car car = new Car(grid);

            // Assert
            intersection.Add(car);

            foreach (var aCar in intersection)
            {
                Assert.IsInstanceOfType(aCar, typeof (Car));
            }
        }// End TestAdd method

        [TestMethod]
        public void TestUpdate()
        {
            // Arrange 
            FixedSignal signal = this.CreateSignal();
            Grid grid = this.CreateGrid(signal);

            List<Vector2> startCoords = new List<Vector2>();
            startCoords.Add(new Vector2(0, 2));
            startCoords.Add(new Vector2(2, 5));
            startCoords.Add(new Vector2(3, 0));
            startCoords.Add(new Vector2(5, 3));
            Intersection intersection = new Intersection(signal, startCoords, grid);

            Car car = new Car(grid);
            Motorcycle moto = new Motorcycle(grid);

            intersection.Add(car);
            intersection.Add(moto);

            car.Move(signal);
            car.Move(signal);
            car.Move(signal);
            car.Move(signal);
            car.Move(signal);

            foreach (var aCar in intersection)
            {
                Assert.IsNotInstanceOfType(aCar, typeof(Car));
            }
        }

        private Grid CreateGrid(ISignalStrategy signal)
        {
            Tile[,] tiles = new Tile[6, 6];

            tiles[0, 0] = new Grass();
            tiles[0, 1] = new Grass();
            tiles[0, 2] = new Road(Direction.Right);
            tiles[0, 3] = new Road(Direction.Left);
            tiles[0, 4] = new Grass();
            tiles[0, 5] = new Grass();

            tiles[1, 0] = new Grass();
            tiles[1, 1] = new Light(signal, Direction.Right);
            tiles[1, 2] = new Road(Direction.Right);
            tiles[1, 3] = new Road(Direction.Left);
            tiles[1, 4] = new Light(signal, Direction.Down);
            tiles[1, 5] = new Grass();

            tiles[2, 0] = new Road(Direction.Down);
            tiles[2, 1] = new Road(Direction.Down);
            tiles[2, 2] = new IntersectionTile();
            tiles[2, 3] = new IntersectionTile();
            tiles[2, 4] = new Road(Direction.Down);
            tiles[2, 5] = new Road(Direction.Down);

            tiles[3, 0] = new Road(Direction.Up);
            tiles[3, 1] = new Road(Direction.Up);
            tiles[3, 2] = new IntersectionTile();
            tiles[3, 3] = new IntersectionTile();
            tiles[3, 4] = new Road(Direction.Up);
            tiles[3, 5] = new Road(Direction.Up);

            tiles[4, 0] = new Grass();
            tiles[4, 1] = new Light(signal, Direction.Up);
            tiles[4, 2] = new Road(Direction.Right);
            tiles[4, 3] = new Road(Direction.Left);
            tiles[4, 4] = new Light(signal, Direction.Left);
            tiles[4, 5] = new Grass();

            tiles[5, 0] = new Grass();
            tiles[5, 1] = new Grass();
            tiles[5, 2] = new Road(Direction.Right);
            tiles[5, 3] = new Road(Direction.Left);
            tiles[5, 4] = new Grass();
            tiles[5, 5] = new Grass();

            Grid grid = new Grid(tiles);

            return grid;
        }

        private FixedSignal CreateSignal()
        {
            // Create FixedSignal
            int[] timing = new int[] { 20, 5, 15, 5 };
            FixedSignal signal = new FixedSignal(timing);

            return signal;
        }
    }// End IntersectionTest class
}
