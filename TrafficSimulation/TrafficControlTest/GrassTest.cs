﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;
using Microsoft.Xna.Framework;

namespace TrafficControlTest
{
    [TestClass]
    public class GrassTest
    {
        [TestMethod]
        public void GrassConstructor()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
           
            // Assert
            Assert.AreEqual(grid[5, 1].Direction, Direction.None);
        }
        private Tile[,] PrepairTile()
        {
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);

            Tile[,] tiles = new Tile[6, 6];

            tiles[0, 0] = new Grass();
            tiles[0, 1] = new Grass();
            tiles[0, 2] = new Road(Direction.Right);
            tiles[0, 3] = new Road(Direction.Left);
            tiles[0, 4] = new Grass();
            tiles[0, 5] = new Grass();

            tiles[1, 0] = new Grass();
            tiles[1, 1] = new Light(signal, Direction.Right);
            tiles[1, 2] = new Road(Direction.Right);
            tiles[1, 3] = new Road(Direction.Left);
            tiles[1, 4] = new Light(signal, Direction.Down);
            tiles[1, 5] = new Grass();

            tiles[2, 0] = new Road(Direction.Down);
            tiles[2, 1] = new Road(Direction.Down);
            tiles[2, 2] = new IntersectionTile();
            tiles[2, 3] = new IntersectionTile();
            tiles[2, 4] = new Road(Direction.Down);
            tiles[2, 5] = new Road(Direction.Down);

            tiles[3, 0] = new Road(Direction.Up);
            tiles[3, 1] = new Road(Direction.Up);
            tiles[3, 2] = new IntersectionTile();
            tiles[3, 3] = new IntersectionTile();
            tiles[3, 4] = new Road(Direction.Up);
            tiles[3, 5] = new Road(Direction.Up);

            tiles[4, 0] = new Grass();
            tiles[4, 1] = new Light(signal, Direction.Up);
            tiles[4, 2] = new Road(Direction.Right);
            tiles[4, 3] = new Road(Direction.Left);
            tiles[4, 4] = new Light(signal, Direction.Left);
            tiles[4, 5] = new Grass();

            tiles[5, 0] = new Grass();
            tiles[5, 1] = new Grass();
            tiles[5, 2] = new Road(Direction.Right);
            tiles[5, 3] = new Road(Direction.Left);
            tiles[5, 4] = new Grass();
            tiles[5, 5] = new Grass();

            return tiles;
        }
    }
}