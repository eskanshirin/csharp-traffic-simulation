﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;
using Microsoft.Xna.Framework;

namespace TrafficControlTest
{
    [TestClass]
    public class FixedSignalTest
    {
        [TestMethod]
        public void TestConstrcutorFixedSignal()
        {

            int[] timing = { 1, 2, 3, 1 };

            FixedSignal a = new FixedSignal(timing);

            Assert.AreEqual(a.GetColour(Direction.Down), Colour.Red);
            Assert.AreEqual(a.GetColour(Direction.Up), Colour.Red);
            Assert.AreEqual(a.GetColour(Direction.Right), Colour.Green);
            Assert.AreEqual(a.GetColour(Direction.Left), Colour.Green);
        }//end con tester

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestGetter()
        {
            /**will be only testing Direction non because tested all the others above**/

            int[] timing = { 1, 2, 3, 1 };

            FixedSignal a = new FixedSignal(timing);

            Assert.Equals(a.GetColour(Direction.None), Colour.Red);
            Assert.Equals(a.GetColour(Direction.None), Colour.Green);
        }//end get tester

        [TestMethod]
        public void TestUpdate()
        {
            /**will be only testing Direction non because tested all the others above**/

            //right left, up down Green/amber  
            int[] timing = { 1, 1, 1, 1};
            FixedSignal a = new FixedSignal(timing);

            a.Update();
            //now the timer increments by 1
            Assert.AreEqual(a.GetColour(Direction.Left), Colour.Green);
            Assert.AreEqual(a.GetColour(Direction.Up), Colour.Red);

           
            a.Update();
            Assert.AreEqual(a.GetColour(Direction.Left), Colour.Amber);
            Assert.AreEqual(a.GetColour(Direction.Up), Colour.Red);
 
           a.Update();
           Assert.AreEqual(a.GetColour(Direction.Left), Colour.Red);
           Assert.AreEqual(a.GetColour(Direction.Up), Colour.Green);

            a.Update();
            Assert.AreEqual(a.GetColour(Direction.Left), Colour.Red);
            Assert.AreEqual(a.GetColour(Direction.Up), Colour.Amber);

            //reset
            a.Update();
            Assert.AreEqual(a.GetColour(Direction.Left), Colour.Green);
            Assert.AreEqual(a.GetColour(Direction.Up), Colour.Red);

        }//end get update
    }//end class
}//end namespace
