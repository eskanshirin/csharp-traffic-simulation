﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TrafficSimulation;

namespace TrafficControlTest
{
    [TestClass]
    public class VehicleCarMotorTest
    {
        [TestMethod]

        public void VehicleConstructor()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            // Assert
            //1-tiles adjust into grid properly
            Assert.AreEqual(grid[0, 0], tiles[0, 0]);
            Assert.AreEqual(grid[3, 2], tiles[3, 2]);

            Assert.AreEqual(car.EmissionIdle, 2);
            Assert.AreEqual(car.EmissionMoving, 5);
            Assert.AreEqual(car.Passengers, 3);
        }

        [TestMethod]
        public void VehicleMotorConstructor()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle motorcycle = new Motorcycle(grid);
            // Assert
            //1-tiles adjust into grid properly
            Assert.AreEqual(grid[0, 0], tiles[0, 0]);
            Assert.AreEqual(grid[3, 1], tiles[3, 1]);

            Assert.AreEqual(motorcycle.EmissionIdle, 1);
            Assert.AreEqual(motorcycle.EmissionMoving, 2);
            Assert.AreEqual(motorcycle.Passengers, 1);
        }

        [TestMethod]
        public void PassesInIntersection()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            car.X = 2;
            car.Y = 2;
            // Assert
            Assert.AreEqual(car.InIntersection(), true);
        }

        [TestMethod]
        public void FaildInIntersection()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            car.X = 3;
            car.Y = 4;
            // Assert
            Assert.AreEqual(car.InIntersection(), false);
        }

        [TestMethod]
        public void PassedNextIsIntersection()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            car.X = 3;
            car.Y = 1;
            // Assert
            Assert.AreEqual(car.NextIsIntersection(), true);

        }

        [TestMethod]
        public void FailedNextIsIntersection()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            car.X = 3;
            car.Y = 4;
            // Assert
            Assert.AreEqual(car.NextIsIntersection(), false);

        }
        [TestMethod]
        public void PassedSignalRedWaitMove()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);
            // Act
            Grid grid = new Grid(tiles);
            Vehicle car = new Car(grid);
            car.X = 3;
            car.Y = 1;
            car.Waiting += moveHandler;
            car.Moved += moveHandler;
            car.Move(signal);
            // Assert
            Assert.AreEqual(car.X, 3);
            Assert.AreEqual(car.Y, 1);

        }

        [TestMethod]
        public void PassedSignalGreenMove()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);
            // Act
            Grid grid = new Grid(tiles);
            Car car2 = new Car(grid);
            car2.X = 1;
            car2.Y = 2;
            car2.Direction = Direction.Right;
            car2.Waiting += moveHandler;
            car2.Moved += moveHandler;
            car2.Move(signal);
            // Assert
            Assert.AreEqual(car2.X, 2);
            Assert.AreEqual(car2.Y, 2);

        }

        [TestMethod]
        public void PassedMoveAccordingToDirection()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);
            Grid grid = new Grid(tiles);
            // Act
            Car car3 = new Car(grid);
            car3.X = 4;
            car3.Y = 2;
            car3.Direction = Direction.Right;
            car3.Waiting += moveHandler;
            car3.Moved += moveHandler;
            car3.Move(signal);
            //Assert
            Assert.AreEqual(car3.X, 5);
            Assert.AreEqual(car3.Y, 2);

        }

        [TestMethod]
        public void PassedIsAvailableWithOtherCar()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);
            Grid grid = new Grid(tiles);
            grid[5, 2].Occupied = true;
            Car car4 = new Car(grid);
            // Act
            car4.X = 4;
            car4.Y = 2;
            car4.Direction = Direction.Right;
            car4.Waiting += moveHandler;
            car4.Moved += moveHandler;

            car4.Move(signal);

            Assert.AreEqual(car4.X, 4);
            Assert.AreEqual(car4.Y, 2);
        }

        [TestMethod]
        public void PassedIsAvailable()
        {
            //Arrange
            Tile[,] tiles = this.PrepairTile();
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);
            Grid grid = new Grid(tiles);
            grid[5, 2].Occupied = true;
            Car car5 = new Car(grid);
            // Act
            car5.X = 0;
            car5.Y = 3;
            car5.Direction = Direction.Left;
            car5.Waiting += moveHandler;
            car5.Moved += moveHandler;
            car5.Done += moveHandler;

            car5.Move(signal);
            //Assert
            Assert.AreEqual(car5.X, 0);
            Assert.AreEqual(car5.Y, 3);
        }


        private void moveHandler(IVehicle v)
        {

        }

        private Tile[,] PrepairTile()
        {
            int[] time = new int[] { 20, 5, 10, 5 };
            FixedSignal signal = new FixedSignal(time);

            Tile[,] tiles = new Tile[6, 6];

            tiles[0, 0] = new Grass();
            tiles[0, 1] = new Grass();
            tiles[0, 2] = new Road(Direction.Right);
            tiles[0, 3] = new Road(Direction.Left);
            tiles[0, 4] = new Grass();
            tiles[0, 5] = new Grass();

            tiles[1, 0] = new Grass();
            tiles[1, 1] = new Light(signal, Direction.Right);
            tiles[1, 2] = new Road(Direction.Right);
            tiles[1, 3] = new Road(Direction.Left);
            tiles[1, 4] = new Light(signal, Direction.Down);
            tiles[1, 5] = new Grass();

            tiles[2, 0] = new Road(Direction.Down);
            tiles[2, 1] = new Road(Direction.Down);
            tiles[2, 2] = new IntersectionTile();
            tiles[2, 3] = new IntersectionTile();
            tiles[2, 4] = new Road(Direction.Down);
            tiles[2, 5] = new Road(Direction.Down);

            tiles[3, 0] = new Road(Direction.Up);
            tiles[3, 1] = new Road(Direction.Up);
            tiles[3, 2] = new IntersectionTile();
            tiles[3, 3] = new IntersectionTile();
            tiles[3, 4] = new Road(Direction.Up);
            tiles[3, 5] = new Road(Direction.Up);

            tiles[4, 0] = new Grass();
            tiles[4, 1] = new Light(signal, Direction.Up);
            tiles[4, 2] = new Road(Direction.Right);
            tiles[4, 3] = new Road(Direction.Left);
            tiles[4, 4] = new Light(signal, Direction.Left);
            tiles[4, 5] = new Grass();

            tiles[5, 0] = new Grass();
            tiles[5, 1] = new Grass();
            tiles[5, 2] = new Road(Direction.Right);
            tiles[5, 3] = new Road(Direction.Left);
            tiles[5, 4] = new Grass();
            tiles[5, 5] = new Grass();

            return tiles;
        }
    }


}
