﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public delegate Boolean Validate(int x, int y);
    public abstract class Vehicle :IVehicle 
    {
        private Grid grid;
        public Direction Direction { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Passengers { get; }
        public double EmissionIdle { get; }
        public double EmissionMoving { get; }
        public event DelVehicle Done;
        public event DelVehicle Moved;
        public event DelVehicle Waiting;
    
        public Vehicle(double emmissionMoving, double emissionIdle, int passengers, Grid grid)
        {
            this.grid = grid;
            this.grid[this.X, this.Y].Occupied = true;
            EmissionMoving = emmissionMoving;
            EmissionIdle = emissionIdle;
            Passengers = passengers;
        }

        public bool InIntersection()
        {
            return (grid[X, Y] is IntersectionTile);

        }

        private bool CheckInstanceOfIntersectionTile(int x, int y)
        {
            return (grid[x,y] is IntersectionTile);

        }

        //how to make it more efficient
        public bool NextIsIntersection()
        {

            if (grid[this.X, this.Y].Direction == Direction.Right)
            {

                return CheckInstanceOfIntersectionTile(this.X, this.Y + 1);

            }
            if (grid[this.X, this.Y].Direction == Direction.Left)
            {

                return CheckInstanceOfIntersectionTile(this.X, this.Y - 1);

            }
            if (grid[this.X, this.Y].Direction == Direction.Down)
            {

                return CheckInstanceOfIntersectionTile(this.X + 1, this.Y);

            }
            if (grid[this.X, this.Y].Direction == Direction.Up)
            {

                return CheckInstanceOfIntersectionTile(this.X - 1, this.Y);

            }
            return true;
        }
        private bool IsAvailable(Validate a, Direction d,int XCheck, int YCheck)
        {
            if (d == Direction.Up)
            {
                return a(XCheck - 1,YCheck);
            }

            if (d == Direction.Down)
            {
                return a(XCheck + 1, YCheck); 

            }

            if (d == Direction.Left)
            {
                return a(XCheck, YCheck - 1); 

            }

            if (d == Direction.Right)
            {
                return a(XCheck, YCheck + 1);

            }
            return false;
        }


        private void MoveAccordingToDirection(Direction d) 
        {
            if (d == Direction.Up)
            {
                this.X--;
                this.grid[this.X, this.Y].Occupied = true;
            }

            if (d == Direction.Down)
            {
                this.X++;
                this.grid[this.X, this.Y].Occupied = true;

            }

            if (d == Direction.Left)
            {
                this.Y--;
                this.grid[this.X, this.Y].Occupied = true;

            }

            if (d == Direction.Right)
            {
                this.Y++;
                this.grid[this.X, this.Y].Occupied = true;

            }
        }

        private void Fucntionality(Direction a)
        {

        }
        public void Move(ISignalStrategy signal)
        {
            //check the next spot of the car
            if (IsAvailable(grid.InBounds,this.Direction, this.X, this.Y))                                
            {
               //your next spot is in the grid, check if no car there
               if(!IsAvailable(grid.IsOccupied, this.Direction, this.X, this.Y))  //negate since if true we dont want move
                {
                    //means that in grid, and a free spot

                    //now check what is in front of me
                    if (NextIsIntersection())
                    {
                        //id have to check what color of light to know if I can move
                        if (signal.GetColour(this.Direction) == Colour.Green)
                        {
                            this.grid[this.X, this.Y].Occupied = false;

                            MoveAccordingToDirection(this.Direction); //Will increment according to direction

                            if (Moved != null)
                            {
                                Moved(this);
                            }
                            
                        }
                        else if ( (signal.GetColour(this.Direction) == Colour.Amber && !InIntersection() ) ||
                            signal.GetColour(Direction) == Colour.Red)
                        {
                            if (Waiting != null)
                            {
                                Waiting(this);
                            }
                            
                        }
                        else if (signal.GetColour(Direction) == Colour.Amber && InIntersection())
                        {
                            this.grid[this.X, this.Y].Occupied = false;

                            MoveAccordingToDirection(this.Direction);

                            if (Moved != null)
                            {
                                Moved(this);
                            }
                        }

                    }//end if NextIsIntersection
                    else
                    {
                        this.grid[this.X, this.Y].Occupied = false;

                        MoveAccordingToDirection(this.Direction); 

                        if (Moved != null)
                        {
                            Moved(this);
                        }
                        
                    }
                   
                }//end if there is a free spot
               //there where no free spots
                else
                {
                  //if there is a car , imma wait
                    if (Waiting != null)
                    {
                        Waiting(this);
                    }
                  
                }
            }//end if in the grid
            else
            {
                this.grid[this.X, this.Y].Occupied = false;
                //cant move +1 any more. Car should be out.
                this.Direction = Direction.None;

                if (Done != null)
                {
                    Done(this);
                }
              
            }
        }//end move
    }// End class
}//end namespace
