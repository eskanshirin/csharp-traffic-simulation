﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public interface ISignalStrategy
    {
        void Update();
        Colour GetColour(Direction dir);
    }
}
