﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle(Grid grid) : base(2, 1, 1, grid)
        { }
       
    }
}
