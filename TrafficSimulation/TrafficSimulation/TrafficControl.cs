﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace TrafficSimulation
{
    public class TrafficControl
    {
        //changed it to public 
        private Random random;
        public int numVehicles { get; private set; }
        public int percentCars{ get; private set; }
        public int percentElectric{ get; private set; }
        public Intersection Intersection { get; private set; }
        public Grid Grid { get; private set; }
        public Total Total { get; private set; }
        private List<Vector2> startCoords = new List<Vector2>();

        public int delay { get; private set; }
        public FixedSignal signal { get; private set; }
        private IVehicle[] vehicles;
        public int indexCarAdded { get; private set; }
        private int indexDelay;

        public TrafficControl() { }

        //indexer for testing 
        public Vector2 this[int index]
        {
            get { return startCoords[index]; }
        }
        public void Parse(string fileContent)
        {
            indexDelay = 0;
            // Create array of lines
            string[] lines = fileContent.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            numVehicles = Int32.Parse(lines[0]);
            delay = Int32.Parse(lines[1]);
            percentCars = Int32.Parse(lines[2]);
            percentElectric = Int32.Parse(lines[3]);

            this.CreateTotal();
            this.CreateSignal(lines);
            this.CreateGrid(lines);

            // Array of vehicles to be added one by one with Intersection.Add
            vehicles = new IVehicle[numVehicles];
            this.CreateVehicleArray();
            this.ShuffleVehiclesArray(vehicles);
            indexCarAdded = 0;

            this.CreateIntersection(lines);
        }

        public void CreateGrid(string[] lines)
        {
            string[] cols = lines[5].Split(' ');
            int rows = lines.Length - 5;
            Tile[,] tiles = new Tile[rows, cols.Length];

            int indexRow = 0;
            for (int i = 5; i < lines.Length; i++)
            {
                // Create array of Letters for each line
                string[] line = lines[i].Split(' ');

                // Iterate through the characters in the line array and see which one is it
                for (int j = 0; j < line.Length; j++)
                {
                    if (line[j].Equals("G"))
                    {
                        tiles[indexRow, j] = new Grass();
                    }
                    else if (line[j].Equals("I"))
                    {
                        tiles[indexRow, j] = new IntersectionTile();
                    }
                    else if (line[j].Equals("U") || line[j].Equals("D") || line[j].Equals("L") || line[j].Equals("R")) 
                    {
                        tiles[indexRow, j] = this.CreateRoadTile(line[j]);
                    }
                    else if (line[j].Equals("1") || line[j].Equals("2") || line[j].Equals("3") || line[j].Equals("4"))
                    {
                        tiles[indexRow, j] = this.CreateLightTile(line[j], this.signal);
                    }
                    else
                    {
                        throw new ArgumentException("Character not in context");
                    }
                }

                indexRow++;
            }

            this.Grid = new Grid(tiles);

        }// End CreateGridMethod

        private Road CreateRoadTile(string letter)
        {
            Road roadTile;

            switch (letter)
            {
                case "U":
                    roadTile = new Road(Direction.Up);
                    break;
                case "D":
                    roadTile = new Road(Direction.Down);
                    break;
                case "R":
                    roadTile = new Road(Direction.Right);
                    break;
                case "L":
                    roadTile = new Road(Direction.Left);
                    break;
                default:
                    throw new ArgumentException("Wrong letter for Road tile!");
            }

            return roadTile;
        }

        private Light CreateLightTile(string number, FixedSignal signal)
        {
            Light lightTile;

            switch (number)
            {
                case "1":
                    lightTile = new Light(signal, Direction.Down);
                    lightTile.Colour = Colour.Red;
                    break;
                case "2":
                    lightTile = new Light(signal, Direction.Left);
                    lightTile.Colour = Colour.Green;
                    break;
                case "3":
                    lightTile = new Light(signal, Direction.Right);
                    lightTile.Colour = Colour.Green;
                    break;
                case "4":
                    lightTile = new Light(signal, Direction.Up);
                    lightTile.Colour = Colour.Red;
                    break;
                default:
                    throw new ArgumentException("Wrong number for Light tile!");
            }

            return lightTile;
        }

        public void CreateTotal()
        {
            this.Total = new Total(numVehicles);
        }

        public void CreateSignal(string[] lines)
        {
            string[] timingStr = lines[4].Split(' ');
            int[] timing = new int[4];

            for (int i = 0; i < timingStr.Length; i++)
            {
                timing[i] = Int32.Parse(timingStr[i]);
            }

            this.signal = new FixedSignal(timing);
        }

        public void CreateIntersection(string[] lines)
        {
            Vector2 entrance1, entrance2, entrance3, entrance4;
            int rows = this.Grid.Size;
            int cols = this.Grid.Size;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    if ( (i == 0) && (((Tile)this.Grid[i, j]).Direction == Direction.Down) )
                    {
                        entrance1 = new Vector2(i, j);
                        startCoords.Add(entrance1);
                    }
                    else if ((i == rows - 1) && (((Tile)this.Grid[i, j]).Direction == Direction.Up))
                    {
                        entrance3 = new Vector2(i, j);
                        startCoords.Add(entrance3);
                    }
                    else if ((j == 0) && (((Tile)this.Grid[i, j]).Direction == Direction.Right))
                    {
                        entrance4 = new Vector2(i, j);
                        startCoords.Add(entrance4);
                    }
                    else if ((j == cols - 1) && (((Tile)this.Grid[i, j]).Direction == Direction.Left))
                    {
                        entrance2 = new Vector2(i, j);
                        startCoords.Add(entrance2);
                    }
                }
            }
            this.Intersection = new Intersection(this.signal, startCoords, this.Grid);

        }// End CreateIntersection method

        public void Update()
        {
            if (indexDelay == 0)
            {
                if (indexCarAdded < numVehicles)
                {
                    

                    Intersection.Add(vehicles[indexCarAdded]);
                    indexCarAdded++;
                }
            }

            indexDelay++;

            if (indexDelay == delay)
            {
                indexDelay = 0;
            }

            Intersection.Update();
        }// End Update method

        public void CreateVehicleArray()
        {
            random = new Random();

            int numCars = (int)(numVehicles * (percentCars * 0.01) );
            int numElectric = (int)(numVehicles * (percentElectric * 0.01));
            int[] electricVehiclesIndex = new int[numElectric];

            // Create an array of int. If the index in the vehicles array corresponds to one of the value found in electricVehiclesIndex, the vehicle will be electric
            for (int index = 0; index < electricVehiclesIndex.Length; index++)
            {
                int num = random.Next(numVehicles);

                // If this number already exist in the array, generate a new int
                while (electricVehiclesIndex.Contains(num))
                {
                    num = random.Next(numVehicles);
                }

                electricVehiclesIndex[index] = num;
            }

            // Populate vehicle array
            for (int index = 0; index < vehicles.Length; index++)
            {
                if (index <= numCars)
                {
                    Car car = new Car(this.Grid);

                    if (electricVehiclesIndex.Contains(index))
                    {
                        vehicles[index] = new Electric(car);

                        vehicles[index].Done += Total.VechicleOver;
                        vehicles[index].Moved += Total.Move;
                        vehicles[index].Waiting += Total.Waiting;
                    }
                    else
                    {
                        vehicles[index] = car;

                        vehicles[index].Done += Total.VechicleOver;
                        vehicles[index].Moved += Total.Move;
                        vehicles[index].Waiting += Total.Waiting;
                    }
                }
                else
                {
                    Motorcycle moto = new Motorcycle(this.Grid);
                    
                    if (electricVehiclesIndex.Contains(index))
                    {
                        vehicles[index] = new Electric(moto);

                        vehicles[index].Done += Total.VechicleOver;
                        vehicles[index].Moved += Total.Move;
                        vehicles[index].Waiting += Total.Waiting;
                    }
                    else
                    {
                        vehicles[index] = moto;

                        vehicles[index].Done += Total.VechicleOver;
                        vehicles[index].Moved += Total.Move;
                        vehicles[index].Waiting += Total.Waiting;
                    }
                }
            }
        } // End CreateVehicleArray method

        private void ShuffleVehiclesArray (IVehicle[] vehiclesArray)
        {
            random = new Random();
            int num;
            IVehicle temp;

            for (int i = 0; i < vehiclesArray.Length; i++)
            {
                num = random.Next(numVehicles);
                temp = vehiclesArray[num];
                vehiclesArray[num] = vehiclesArray[i];
                vehiclesArray[i] = temp;
            }
        }// End ShuffleVehiclesArray method
    }// end TrafficControl class
}
