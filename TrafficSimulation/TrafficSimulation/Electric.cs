﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public class Electric : IVehicle
    {
        IVehicle Vehicle;

        public event DelVehicle Done;
        public event DelVehicle Moved;
        public event DelVehicle Waiting;

        public Electric(IVehicle vehicle)
        {
            this.Vehicle = vehicle;
            this.Vehicle.Moved += this.Moved;
            this.Vehicle.Done += this.Done;
            this.Vehicle.Waiting += this.Waiting;
        }

        public Direction Direction
        {
            get { return this.Vehicle.Direction; }
            set { this.Vehicle.Direction = value; }
        }

        public int X
        {
            get { return this.Vehicle.X; }
            set { this.Vehicle.X = value; }
        }

        public int Y
        {
            get { return this.Vehicle.Y; }
            set { this.Vehicle.Y = value; }
        }

        public int Passengers
        {
            get { return this.Vehicle.Passengers; }
        }

        public double EmissionMoving
        {
            get { return this.Vehicle.EmissionMoving * 0.25; }
        }

        public double EmissionIdle
        {
            get { return 0; }
        }

        public void Move(ISignalStrategy signal)
        {
            this.Vehicle.Move(signal);
        }

        public bool NextIsIntersection()
        {
            return this.Vehicle.NextIsIntersection();
        }

        public bool InIntersection()
        {
            return this.Vehicle.InIntersection();
        }
    }// End class
}
