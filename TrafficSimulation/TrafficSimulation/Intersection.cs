﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace TrafficSimulation
{
    public class Intersection : IEnumerable<IVehicle>
    {
       public  List<IVehicle> vehicles = new List<IVehicle>();
       public List<Vector2> startCoords = new List<Vector2>();
       public Grid grid;
       public ISignalStrategy signal;
       Random random = new Random();
        public Intersection()
        {
            // populate possible start coordinates
            startCoords.Add(new Vector2(1, 0));
            startCoords.Add(new Vector2(2, 0));
            startCoords.Add(new Vector2(0, 1));
            startCoords.Add(new Vector2(0, 2));

        }

        public Intersection(ISignalStrategy signal, List<Vector2> startCoords1, Grid grid)
        {
            this.signal = signal;
            this.grid = grid;
            for (int i = 0; i < startCoords1.Count; i++)
            {
                if (grid[(int)startCoords1[i].X, (int)startCoords1[i].Y].Occupied)
                {
                    throw new ArgumentException("Start Coords is occupied");
                }
                startCoords.Add(startCoords1.ElementAt(i));
            }


        }


        public void Update()
        {
            foreach (IVehicle vehicle in vehicles.ToList())
            {
                vehicle.Move(signal);
            }
            signal.Update();
            
            for (int i = 0; i < this.grid.Size; i++)
            {
                for (int j = 0; j < this.grid.Size; j++)
                {
                    if (this.grid[i, j] is Light)
                    {
                        if ((((Light)this.grid[i, j]).Direction == Direction.Down) || (((Light)this.grid[i, j]).Direction == Direction.Up))
                        {
                            ((Light)this.grid[i, j]).Colour = signal.GetColour(Direction.Down);
                        }
                        else if ((((Light)this.grid[i, j]).Direction == Direction.Right) || (((Light)this.grid[i, j]).Direction == Direction.Left))
                        {
                            ((Light)this.grid[i, j]).Colour = signal.GetColour(Direction.Right);
                        }
                    }
                    
                }
            }
        }

        public void Add(IVehicle vehicle)
        {
            // find a random position for Vehicle
            int positionIndex = random.Next(0, startCoords.Count);
            Vector2 position = startCoords[positionIndex];
            // continue until random position is vacant
            while (grid[(int)position.X, (int)position.Y].Occupied)
            {
                positionIndex = random.Next(0, startCoords.Count);
                position = startCoords[positionIndex];
            }

            vehicle.X = (int)position.X;
            vehicle.Y = (int)position.Y;
            vehicle.Direction = grid[(int)position.X, (int)position.Y].Direction;

            //Subscribe an event handler to done event of vehicle
            vehicle.Done += removeFomIntersection;
            vehicles.Add(vehicle);
        }

        private void removeFomIntersection(IVehicle vehicle)
        {
            vehicles.Remove(vehicle);
        }

        public IEnumerator<IVehicle> GetEnumerator()
        {
            return vehicles.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        public IVehicle this[int i]
        {
            get {
                return vehicles[i];
            }
        }
    }
}
//method in my test code, add that method to my event
//this method will add 1 to a counter so if counter is 1 you know your evetn