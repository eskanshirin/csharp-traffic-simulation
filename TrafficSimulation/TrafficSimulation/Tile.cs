﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public abstract class Tile
    {
        public Direction Direction { get; set; }
        public Boolean Occupied { get; set; }

        public Tile(Direction d)
        {
            this.Direction = d;
        }

    }//end Tile
}//end nameSpace
