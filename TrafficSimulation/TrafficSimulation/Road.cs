﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public class Road : Tile
    {
        public Road(Direction d) : base (d)
        { }
    }
}
