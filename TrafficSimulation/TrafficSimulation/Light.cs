﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
   public enum Colour
    {
        Red,
        Green,
        Amber
    }

    public class Light : Tile
    {
        private ISignalStrategy strategy;
        public Light(ISignalStrategy member, Direction d) : base(d)
        {
            this.strategy = member;
        
        }

    public Colour Colour { get; set; }
}
}
    