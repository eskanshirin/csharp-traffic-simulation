﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public delegate void DelVehicle(IVehicle b);


    public interface IVehicle
    {
        // Events
        event DelVehicle Done;
        event DelVehicle Moved;
        event DelVehicle Waiting;

        // Properties
        Direction Direction { get; set; }
        int X { get; set; }
        int Y { get; set; }
        int Passengers { get; }
        double EmissionIdle { get; }
        double EmissionMoving { get; }

        // Methods
        void Move(ISignalStrategy signal);
        Boolean NextIsIntersection();
        Boolean InIntersection();
    }
}
