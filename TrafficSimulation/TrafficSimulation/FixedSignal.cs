﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public class FixedSignal : ISignalStrategy
    {
        private int[] timing;
        private int currentIndex;
        private Colour upDown;
        private Colour rightLeft;

        public FixedSignal(params int[] timing)
        {
            this.timing = timing;
            this.currentIndex = 0;
            this.upDown = Colour.Red;
            this.rightLeft = Colour.Green;
        }
        public Colour GetColour(Direction dir)
        {

            if (dir == Direction.Down || dir == Direction.Up)
            {
                return this.upDown;
            }
            else if(dir == Direction.Right || dir == Direction.Left)
            {
                return this.rightLeft;
            }
            else
            {
                throw new ArgumentException("The direction provided is not a light!");
            }
        }

        public void Update()
        {

            this.currentIndex++;
            //restart!
            
            if (currentIndex <= timing[0])
            {
                rightLeft = Colour.Green;
                upDown = Colour.Red;
            }
            else if  (currentIndex > timing[0] && currentIndex <= timing[0] + timing[1])
            {
                rightLeft = Colour.Amber;
                upDown = Colour.Red;
            }

            else if (currentIndex > timing[0] + timing[1] && currentIndex <= timing[0] + timing[1] + timing[2])
            {
                rightLeft = Colour.Red;
                upDown = Colour.Green;
            }
            else if (currentIndex > timing[0] + timing[1] + timing[2] && currentIndex <= timing[0] + timing[1] + timing[2] + timing[3])
            { 
                rightLeft = Colour.Red;
                upDown = Colour.Amber;
            }

            if (currentIndex == timing.Sum())
            {
                currentIndex = 0;
            }
        }// end Update method

        public String TimingToString()
        {
            String s="";
            for(int i = 0; i < timing.Length; i++)
            {
                if (i == timing.Length - 1)
                {
                    s = s + timing[i];
                }
                else
                {
                  s = s + timing[i] + " ";
                }
            }
            return s; //toString() in array does the same?!
        }
    }// End FixedSignal class
}
