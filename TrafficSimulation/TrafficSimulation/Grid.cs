﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrafficSimulation
{
    public class Grid
    {
        private Tile[,] grid;

        public Grid(Tile[,] tiles)
        {
            if (tiles.GetLength(0) < 4 || tiles.GetLength(1) < 4)
            {
                throw new ArgumentException("The grid privided is too small!");
            }

            if (tiles.GetLength(0) != tiles.GetLength(1))
            {
                throw new ArgumentException("The grid provided must be square");
            }

            this.grid = new Tile[tiles.GetLength(0), tiles.GetLength(1)];

            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    this.grid[i, j] = tiles[i, j];
                }
            }
        }

        // Indexer
        public Tile this[int row, int col]
        {
            get { return this.grid[row, col]; }
        }

        public int Size
        {
            get { return this.grid.GetLength(0); }
        }

        public bool IsOccupied (int x, int y)
        {
            return this.grid[x, y].Occupied;
        }

        // Check if coordinates provided are within grid
        public bool InBounds (int x, int y)
        {
            if ( x < 0 || x >= this.grid.GetLength(0)|| y < 0 || y >= this.grid.GetLength(1) )
            {
                return false;
            }

            return true;
        }
    } // End class
} // End namespace
